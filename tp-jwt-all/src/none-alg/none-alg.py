import jwt
import json
import base64

secret_key = "cds@kclsdlkaslklkcaldklkc@dsalkcdsclkdslkds@@"

def user(username):
    if(not username == ""):
        payload = {"username": username, "is_admin": False}
        token = jwt.encode(payload, secret_key, algorithm="HS256")
        return token
    else:
        return "Invalid username"

def check_token(token):
    try:
        payload = jwt.decode(token, secret_key, algorithms=["HS256"])
        if payload["is_admin"]:
            return "Congrats ! The next challenge url is: /f91c2979fd539a16c2098c812a9973ab"
        else:
            return "Error: user " + payload["username"] + " is not admin"
    except jwt.exceptions.InvalidAlgorithmError:
        header = jwt.get_unverified_header(token)
        try : 
            if(header['alg'] == "none"):
                payload =jwt.decode(token,options={"verify_signature": False})
                try : 
                    if(payload['is_admin']):
                        return "Congrats ! The next challenge url is: /f91c2979fd539a16c2098c812a9973ab"
                    else : 
                        return '{ You are so close ... }'
                except Exception as e: 
                    return "Invalid token: " + str(e)
            else : 
                return '{ You are so close ... }'
        except Exception as e: 
            return "Invalid token: " + str(e)   
    except Exception as e:
        return "Invalid token: " + str(e)

def lambda_handler(event, context):
    print(json.dumps(event))
    path = event['requestContext']['http']['path']
    method = event['requestContext']['http']['method']       

    # GET method
    if(method == 'GET'):
        if(path == '/'):
            body = 'Hi welcome to my diy JWT implementation built with old random code I found on the web ! You can POST {"username": "example"} to /user but _none_ shall be able to authenticate as admin in /admin as it is secure'
        elif(path == '/admin' or path == '/user'):
            body = "POST not GET ;)"
        else:
            body = {'message': 'Invalid path'}

    # POST method
    elif(method == 'POST'):
        # Decode payload and parse JSON
        try:
            payload = json.loads(base64.b64decode(event['body']))
            print(json.dumps(payload))
        except Exception as e:
            body = "Error decoding payload: " + str(e)

        if(path == '/user'):
            try:
                body = {"token": user(payload['username'])}
            except Exception as e:
                body = "Error on /user: " + str(e)
        elif(path == '/admin'):
            try:
                print("Token is: " + event['headers']['authorization'])
                body = check_token(event['headers']['authorization'])
            except Exception as e:
                body = "Error on /admin, missing 'Authorization' header ? " + str(e)
        else:
            body = {'message': 'Invalid path'}
    else:
        body = {'message': 'Invalid HTTP method'}
    
    response = {
        'statusCode': 200,
        'body': json.dumps(body)
    }
    
    return response