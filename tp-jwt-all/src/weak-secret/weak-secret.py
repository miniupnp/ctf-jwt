import jwt
import json
import base64

secret_key = "your-256-bit-secret"

def user(username):
    if(not username == ""):
        payload = {"username": username, "is_admin": False}
        token = jwt.encode(payload, secret_key, algorithm="HS256")
        return token
    else:
        return "Invalid username"

def check_token(token):
    try:
        payload = jwt.decode(token, secret_key, algorithms=["HS256"])
        if payload["is_admin"]:
            return "Congrats ! The next challenge url is: /03f21b939e022176e87feb7704477bbc"
        else:
            return "Error: user " + payload["username"] + " is not admin"
    except Exception as e:
        return "Invalid token: " + str(e)

def lambda_handler(event, context):
    print(json.dumps(event))
    path = event['requestContext']['http']['path']
    method = event['requestContext']['http']['method']       

    # GET method
    if(method == 'GET'):
        if(path == '/f91c2979fd539a16c2098c812a9973ab'):
            body = 'Hi welcome to the most diy secure JWT implementation built in collaboration with ChatGPT and ****->JWT.IO<-**** ! You can POST {"username": "example"} to /user and this time you will not be able to authentiate as admin in /admin Eheheh'
        elif(path == '/f91c2979fd539a16c2098c812a9973ab/admin' or path == '/f91c2979fd539a16c2098c812a9973ab/user'):
            body = "POST not GET ;)"
        else:
            body = {'message': 'Invalid path'}

    # POST method
    elif(method == 'POST'):
        # Decode payload and parse JSON
        try:
            payload = json.loads(base64.b64decode(event['body']))
            print(json.dumps(payload))
        except Exception as e:
            body = "Error decoding payload: " + str(e)

        if(path == '/f91c2979fd539a16c2098c812a9973ab/user'):
            try:
                body = {"token": user(payload['username'])}
            except Exception as e:
                body = "Error on /user: " + str(e)
        elif(path == '/f91c2979fd539a16c2098c812a9973ab/admin'):
            try:
                print("Token is: " + event['headers']['authorization'])
                body = check_token(event['headers']['authorization'])
            except Exception as e:
                body = "Error on /admin, missing 'Authorization' header ? " + str(e)
        else:
            body = {'message': 'Invalid path'}
    else:
        body = {'message': 'Invalid HTTP method'}
    
    response = {
        'statusCode': 200,
        'body': json.dumps(body)
    }
    
    return response